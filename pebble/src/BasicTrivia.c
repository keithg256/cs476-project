#include <pebble.h>

Window *title_window, *choices_window, *correct_window, *incorrect_window, *final_score_window;
TextLayer *title_text_layer, *top_choice_text_layer, *middle_choice_text_layer, *bottom_choice_text_layer, *correct_text_layer, *incorrect_text_layer, *final_score_header_text_layer, *final_score_text_layer;

//answers: 0 = top, 1 = middle, 2 = bottom
int answerIndicies[] = {1,0,1,0,2,2,1,1,0,2,1,1,0,2,2,0,2,1,0,1,2,2,0,0,1,2,2,1,0,0,2,1,0,1,2,0,1,2,0,0,2,1,2,0,1,0,2,0,1,2,0,1,2,0,1,1,0,2,2,0};

int questionNumber = 0;
int totalQuestions = 50;
int points;
bool returningForMore = false;
char *string = "    ";

//answers as strings
const char * const answers1[] = { "One hundred","Cheetah","Three","Red","Hale Bopp","Oxygen","Mickey Mouse","Five","James","Chocolate","Toddler","Five","Baloo","Thomas IV","A shark tooth","Hedwig","St, Joseph", "Dublin","Robin","Dinosaur","Friar Mark","Waffles","Summer","Seven","Steak","Brazil","Cabbage","Watermelons","Pacific","Magma","Geographer","Mount St. Helens","Oxygen","Agoraphobia","Bulls","Deer","Six","Hamstrings","Skin","Vertebrae","Cylinder","Fish","0 degrees F","Nile","Erosion","Three","Armageddon","Circumference","China","Mexico","South Africa","Peru","Programming languages","Smell","Saturn","Three","Zero","Weather","Carnivores","Extinction"};
const char * const answers2[] = { "One thousand","Tiger","Four","Blue","Sirius","Nitrogen","Donald Duck","Six","John","Jelly","Kid","Seven","Mowgli","Philip III","A necklace","Dobby","St. Peter","London","Clepto","Horse","Friar Matthew","Beignets","Winter","Eleven","Venison","China","Sprouts","Pumpkins","Atlantic","Lava","Geocacher","Mt Everest","Hydrogen","Arachnophobia","Oxen","Turtle","Eight","Calves","Heart","Ligaments","Dome","Clouds","20 degrees F","Amazon","Photosynthesis","Four","Toy Story","Radius","India","Portugal","Switzerland","Egypt","Operating Systems","Sight","Jupiter","Seven","One","Animals","Herbivores","Extortion"};
const char * const answers3[] = { "One million","Giraffe","Five","Green","Milky Way","Hydrogen","Bugs Bunny","Seven","Jacob","Honey","Pup","Nine","Bagheera","Henry VIII","A tie","Hermione","St. Patrick","Istanbul","Shrek","Dog","Friar Tuck","Crepes","Spring","Thirteen","Gumbo","India","Corn","Potatoes","Arctic","Fire","Geologist","Mount Vesuvius","Nitrogen","Claustrophobia","Cows","Wombat","Ten","Quadriceps","Brain","Tendons","Double helix","Body parts","32 degrees F","Ganges","Carbonation","Five","Jurassic Park","Diameter","Russia","Spain","Scotland","Argentina","Web browsers","Sound","Venus","Twelve","Four","Plants","Omnivores","Excommunication"};

bool first = true;

void long_override_back_handler(ClickRecognizerRef ref, void* context) {
    window_stack_pop_all(true);
}

void short_override_back_handler(ClickRecognizerRef ref, void* context) {
}

void override_back_click_config(void *window) {
    window_long_click_subscribe(BUTTON_ID_BACK, 2000, long_override_back_handler, NULL);
    window_single_click_subscribe(BUTTON_ID_BACK, short_override_back_handler);
}


void exit_game_handler(ClickRecognizerRef ref, void *context) {
    window_stack_pop_all(true);
}

void exit_game_click_config(void *window) {
    window_single_click_subscribe(BUTTON_ID_SELECT, exit_game_handler);
}

void final_score_window_load(Window* window) {
    Layer* window_layer = window_get_root_layer(window);
    
    final_score_header_text_layer = text_layer_create(GRect(5, 30, 144, 168));
    text_layer_set_text_alignment(final_score_header_text_layer, GTextAlignmentCenter);
    text_layer_set_font(final_score_header_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
    text_layer_set_text(final_score_header_text_layer, "Final score:");
    layer_add_child(window_layer, text_layer_get_layer(final_score_header_text_layer));
    
    final_score_text_layer = text_layer_create(GRect(5, 70, 144, 168));
    text_layer_set_text_alignment(final_score_text_layer, GTextAlignmentCenter);
    text_layer_set_font(final_score_text_layer, fonts_get_system_font(FONT_KEY_BITHAM_42_BOLD));
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "string for points DURING LOAD TIME:");
    text_layer_set_text(final_score_text_layer, string);
    layer_add_child(window_layer, text_layer_get_layer(final_score_text_layer));
}

void final_score_window_unload(Window* window) {
    text_layer_destroy(final_score_text_layer);
}

void pop_correctness_window_handler(ClickRecognizerRef ref, void *context) {
    window_stack_pop(true);
}

void next_question_click_config(void *window) {
    if(questionNumber < totalQuestions) {
        window_single_click_subscribe(BUTTON_ID_SELECT, pop_correctness_window_handler);
    }
}

void incorrect_window_load(Window* window) {
    Layer* window_layer = window_get_root_layer(window);
    //here send the phone dictionary with a '0' denoting incorrect
    DictionaryIterator *iter;
    app_message_outbox_begin(&iter);
    
    questionNumber++;
    Tuplet tuple = TupletCString(5, "n");//5 is key to parse from other side, n is the data
    dict_write_tuplet(iter, &tuple);
    dict_write_end(iter);
    
    app_message_outbox_send();
    
    incorrect_text_layer = text_layer_create(GRect(0, 60, 144, 168));
    text_layer_set_text_alignment(incorrect_text_layer, GTextAlignmentCenter);
    text_layer_set_font(incorrect_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
    text_layer_set_text(incorrect_text_layer, "Incorrect!");
    layer_add_child(window_layer, text_layer_get_layer(incorrect_text_layer));
}

void incorrect_window_unload(Window* window) {
    text_layer_destroy(incorrect_text_layer);
}

void correct_window_load(Window* window) {
    Layer* window_layer = window_get_root_layer(window);
    //here send the phone dictionary with a '1' denoting correct
    DictionaryIterator *iter;
    app_message_outbox_begin(&iter);
    questionNumber++;
    Tuplet tuple = TupletCString(5, "y");//5 is key to parse from other side, y is the data
    dict_write_tuplet(iter, &tuple);
    dict_write_end(iter);
    
    app_message_outbox_send();
    
    correct_text_layer = text_layer_create(GRect(0, 60, 144, 168));
    text_layer_set_text_alignment(correct_text_layer, GTextAlignmentCenter);
    text_layer_set_font(correct_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
    text_layer_set_text(correct_text_layer, "Correct!");
    layer_add_child(window_layer, text_layer_get_layer(correct_text_layer));
}

void correct_window_unload(Window* window) {
    text_layer_destroy(correct_text_layer);
}

void select_top_choice_handler(ClickRecognizerRef ref, void* context) {
    if(answerIndicies[questionNumber] == 0) {
        window_stack_push(correct_window, true);
    } else {
        window_stack_push(incorrect_window, true);
    }
}

void select_middle_choice_handler(ClickRecognizerRef ref, void* context) {
    if(answerIndicies[questionNumber] == 1) {
        window_stack_push(correct_window, true);
    } else {
        window_stack_push(incorrect_window, true);
    }
}

void select_bottom_choice_handler(ClickRecognizerRef ref, void* context) {
    if(answerIndicies[questionNumber] == 2) {
        window_stack_push(correct_window, true);
    } else {
        window_stack_push(incorrect_window, true);
    }
}

void choices_click_config(void *window) {
    window_single_click_subscribe(BUTTON_ID_UP, select_top_choice_handler);
    window_single_click_subscribe(BUTTON_ID_SELECT, select_middle_choice_handler);
    window_single_click_subscribe(BUTTON_ID_DOWN, select_bottom_choice_handler);
    window_long_click_subscribe(BUTTON_ID_BACK, 2000, long_override_back_handler, NULL);
    window_single_click_subscribe(BUTTON_ID_BACK, short_override_back_handler);
}

void choices_window_load(Window* window) {
    Layer* window_layer = window_get_root_layer(window);
    
    top_choice_text_layer = text_layer_create(GRect(0,5,144,168));
    text_layer_set_text_alignment(top_choice_text_layer, GTextAlignmentCenter);
    text_layer_set_font(top_choice_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
    text_layer_set_text(top_choice_text_layer, answers1[questionNumber]);
    layer_add_child(window_layer, text_layer_get_layer(top_choice_text_layer));
    
    middle_choice_text_layer = text_layer_create(GRect(0,60,144,168));
    text_layer_set_text_alignment(middle_choice_text_layer, GTextAlignmentCenter);
    text_layer_set_font(middle_choice_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
    text_layer_set_text(middle_choice_text_layer, answers2[questionNumber]);
    layer_add_child(window_layer, text_layer_get_layer(middle_choice_text_layer));
    
    bottom_choice_text_layer = text_layer_create(GRect(0,115,144,168));
    text_layer_set_text_alignment(bottom_choice_text_layer, GTextAlignmentCenter);
    text_layer_set_font(bottom_choice_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
    text_layer_set_text(bottom_choice_text_layer, answers3[questionNumber]);
    layer_add_child(window_layer, text_layer_get_layer(bottom_choice_text_layer));
}

void choices_window_unload(Window* window) {
    text_layer_destroy(top_choice_text_layer);
    text_layer_destroy(middle_choice_text_layer);
    text_layer_destroy(bottom_choice_text_layer);
}

void push_choice_window_handler(ClickRecognizerRef ref, void *context) {
    window_stack_push(choices_window, true);
}

void title_window_load(Window* window) {
    Layer* window_layer = window_get_root_layer(window);
    
    title_text_layer = text_layer_create(GRect(0, 60, 144, 168));
    text_layer_set_text_alignment(title_text_layer, GTextAlignmentCenter);
    text_layer_set_font(title_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
    text_layer_set_text(title_text_layer, "Triviapalooza!");
    layer_add_child(window_layer, text_layer_get_layer(title_text_layer));
}

void title_window_unload(Window* window) {
    text_layer_destroy(title_text_layer);
}


static void in_received_handler(DictionaryIterator *iter, void *context) {
    
    bool exit = false;
    
    Tuple *tuple = dict_read_first(iter);
    switch (tuple->key){
        case 0:
            window_stack_pop_all(true);
            break;
        case 2:
            string = tuple->value->cstring;
            questionNumber = 0;
            exit = true;
            returningForMore = true;
            window_stack_push(final_score_window, true);
            exit = true;
            break;
        case 10:
            totalQuestions = 10;
            break;
        case 20:
            totalQuestions = 20;
            break;
        case 30:
            totalQuestions = 30;
            break;
        case 40:
            totalQuestions = 40;
            break;
        case 50:
            totalQuestions = 50;
            break;
    }
    
    if(exit){
        APP_LOG(APP_LOG_LEVEL_DEBUG, "EXITTED INSTEAD OF POP/PUSH");
    }
    else{
        if(!first){
            if(returningForMore){
                returningForMore = false;
                window_stack_pop(true);//pop off ""correct
            }
            
            window_stack_pop(true);//pop off ""correct
            window_stack_pop(true);//pop off previous answers
            //push on
            window_stack_push(choices_window, true);
            
        }
        else{
            //iOS displayed a question, show the answers.
            first = false;
            window_stack_push(choices_window, true);
        }
    }
}


static void in_dropped_handler(AppMessageResult reason, void *context) {
}

static void out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context) {
}

void init() {
    
    // Register message handlers
    app_message_register_inbox_received(in_received_handler);
    app_message_register_inbox_dropped(in_dropped_handler);
    app_message_register_outbox_failed(out_failed_handler);
    
    // Init buffers
    app_message_open(64, 64);
    
    title_window = window_create();
    window_set_window_handlers(title_window, (WindowHandlers) {
        .load = title_window_load,
        .unload = title_window_unload,
    });
    
    choices_window = window_create();
    window_set_window_handlers(choices_window, (WindowHandlers) {
        .load = choices_window_load,
        .unload = choices_window_unload,
    });
    window_set_click_config_provider(choices_window, choices_click_config);
    
    correct_window = window_create();
    window_set_window_handlers(correct_window, (WindowHandlers) {
        .load = correct_window_load,
        .unload = correct_window_unload,
    });
    window_set_click_config_provider(correct_window, override_back_click_config);
    
    incorrect_window = window_create();
    window_set_window_handlers(incorrect_window, (WindowHandlers) {
        .load = incorrect_window_load,
        .unload = incorrect_window_unload,
    });
    window_set_click_config_provider(incorrect_window, override_back_click_config);
    
    final_score_window = window_create();
    window_set_window_handlers(final_score_window, (WindowHandlers) {
        .load = final_score_window_load,
        .unload = final_score_window_unload,
    });
    window_set_click_config_provider(final_score_window, override_back_click_config);
    
    window_stack_push(title_window, true);
}

void deinit() {
    window_destroy(title_window);
}

int main() {
    init();
    app_event_loop();
    deinit();
}
