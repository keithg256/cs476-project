//
//  kViewController.h
//  Triviapalooza
//
//  Created by Keith Gardner on 4/29/15.
//  Copyright (c) 2015 Keith Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>



@interface kViewController : GLKViewController<GLKViewDelegate, GLKViewControllerDelegate>

@property(nonatomic, strong) GLKBaseEffect * effect;
@property(nonatomic, strong) EAGLContext * ctx;
@property(nonatomic, strong) GLKView * graphicViewer;

//render the image
-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect;
-(void)glkViewControllerUpdate:(GLKViewController *)controller;


@end