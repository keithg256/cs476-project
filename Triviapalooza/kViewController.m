//
//  kViewController.m
//  Triviapalooza
//
//  Created by Keith Gardner on 4/29/15.
//  Copyright (c) 2015 Keith Gardner. All rights reserved.
//

#import "kViewController.h"
#import <GLKit/GLKit.h>


@interface kViewController ()

@end

@implementation kViewController

@synthesize ctx, effect, graphicViewer;


//pops to home screen after a touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Free unused resources like images and cached data
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //designate openGL ES 2.0
    ctx = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    // Assign openGL context to the view loaded from storyboard
    graphicViewer = (GLKView *) self.view;
    graphicViewer.context = ctx;
    graphicViewer.delegate = self;
    
    //Configure renderbuffers created by the view
    //Changes color format to Red,Green,Blue,Alpha. Alpha is level of transparency
    graphicViewer.drawableColorFormat = GLKViewDrawableColorFormatRGBA8888;
    //Formats stencil renderbuffer
    graphicViewer.drawableStencilFormat = GLKViewDrawableStencilFormat8;
    //Sets 16-bit entry depth per pixel
    graphicViewer.drawableDepthFormat = GLKViewDrawableDepthFormat16;
    
    //Sets FPS
    self.preferredFramesPerSecond = 60;
    self.delegate=self;
    effect = [[GLKBaseEffect alloc] init];
}


//bind framebuffer as the target for OpenGL ES rendering commands
//then draw the view’s contents
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect{
    [effect prepareToDraw];
    
    //sets up the shape of our arrowheads by forming triangles
    static const GLfloat Points[] = {
        1.0f, -1.0f, 1,
        0.5f, -0.5f, 1,
        -0.5f,  0.5f, 1,
        0.5f,  0.5f, 1,
        0.5f, 2.0f, 1
    };
    
    
    //Sets colors at the vertices. We chose bright colors
    static const GLubyte Colors[] = {
        255, 225,   0, 225, //yellow
        0,   255, 255, 255, //light blue
        0,   255,   0,   255, //green
        225,   0, 255, 255, //pink
    };
    
    //Clears our buffers
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
    glClear(GL_STENCIL_BUFFER_BIT);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glEnableVertexAttribArray(GLKVertexAttribColor);
    
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 0, Points);
    glVertexAttribPointer(GLKVertexAttribColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, Colors);
    
    //Draw a triangle fan starting at index 0 consisting of 10 vertices
    glDrawArrays(GL_TRIANGLE_FAN, 0, 10);
    
    glDisableVertexAttribArray(GLKVertexAttribPosition);
    glDisableVertexAttribArray(GLKVertexAttribColor);
}

- (void)glkViewControllerUpdate:(GLKViewController *)controller{
    static float posY = 0.0f;
    float y = sinf(posY)/1.5f;//put bounds on y position via sinf
    posY += 0.01f;//speed of vertical movement
    
    GLKMatrix4 modelviewmatrix  = GLKMatrix4MakeTranslation(0, y, -5.0f);
    effect.transform.modelviewMatrix = modelviewmatrix;
    
    GLfloat ratio = self.view.bounds.size.width/self.view.bounds.size.height;
    GLKMatrix4 projectionmatrix = GLKMatrix4MakePerspective(45.0f, ratio, 0.1f, 20.0f);
    effect.transform.projectionMatrix = projectionmatrix;
    
}

- (void)viewDidUnload
{
    [self setGraphicViewer:nil];
    [EAGLContext setCurrentContext:nil];
    [self setCtx:nil];
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSLog(@"activate timer");
    
    [NSTimer scheduledTimerWithTimeInterval:8
                                     target:self
                                   selector:@selector(go)
                                   userInfo:nil
                                    repeats:NO];
    
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) go{
    [self performSegueWithIdentifier:@"go" sender:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


@end
