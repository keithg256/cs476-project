//
//  AppDelegate.h
//  Triviapalooza
//
//  Created by Keith Gardner on 3/30/15.
//  Copyright (c) 2015 Keith Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

