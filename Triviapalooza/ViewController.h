//
//  ViewController.h
//  Triviapalooza
//
//  Created by Keith Gardner on 3/30/15.
//  Copyright (c) 2015 Keith Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PebbleKit.h"

@interface ViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@end

PBWatch* watch;

NSArray *_pickerData;
NSArray *questions;
NSArray *answers1;
NSArray *answers2;
NSArray *answers3;

int answerIndicies[];

int numQuestions = 10;
int current = 0;
int points = 0;
bool game;

NSTimer *timer;
int remainingCounts = 10;