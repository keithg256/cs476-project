//
//  ViewController.m
//  Triviapalooza
//
//  Created by Keith Gardner on 3/30/15.
//  Copyright (c) 2015 Keith Gardner. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UILabel *quesiton;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UILabel *points;
@property (weak, nonatomic) IBOutlet UIView *feedbackview;
@property (weak, nonatomic) IBOutlet UILabel *feedbacklabel;
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UILabel *score;

@end

@implementation ViewController

bool first = false;

-(void)countDown {
    //out of time
    if(!remainingCounts){
        [self.navigationController popViewControllerAnimated:YES];
        game = false;
        [timer invalidate];
    }
    
    NSString *timeStr = [NSString stringWithFormat:@"%d",remainingCounts];
    _timerLabel.text = timeStr;

    if (--remainingCounts == -1) {
        [timer invalidate];
        
        NSLog(@"Just ran down to 0.");
        
        //send message to pop back to home screen
        NSDictionary *sendQ = @{ @(0):[NSNumber numberWithUint8:1]};
        
        [watch appMessagesPushUpdate:sendQ onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
            if (!error) {
                NSLog(@"Successfully sent message.");
            }
            else {
                NSLog(@"Error sending message: %@", error);
            }
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"view did appear. game: %d", game);
    
    if(game){
        _quesiton.text = questions[current];
        
        //send number of questions
        NSDictionary *sendQ = @{ @(numQuestions):[NSNumber numberWithUint8:1]};
        
        [watch appMessagesPushUpdate:sendQ onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
            if (!error) {
                NSLog(@"Successfully sent message.");
            }
            else {
                NSLog(@"Error sending message: %@", error);
            }
        }];
        
        
        //say start
        NSDictionary *update = @{ @(0):[NSNumber numberWithUint8:42]};
        
        [watch appMessagesPushUpdate:update onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
            if (!error) {
                NSLog(@"Successfully sent message.");
            }
            else {
                NSLog(@"Error sending message: %@", error);
            }
        }];
        
        NSString *pointSrt = [NSString stringWithFormat:@"%d",points];
        _points.text = pointSrt;
        remainingCounts = 10;//set timer to 10 seconds left
        
        _timerLabel.text = @"10";
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                 target:self
                                               selector:@selector(countDown)
                                               userInfo:nil
                                                repeats:YES];
        
    }
    else{//we are going home. reset current number of questions
        current = 0;
        points = 0;
        //numQuestions = 10;//ALERT TODO
    }
}

- (IBAction)continue:(id)sender {
    //unhide feedback view
    _feedbackview.hidden = YES;
    
    //reset time to 10 seconds
    _timerLabel.text = @"10";
    remainingCounts = 10;

    timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(countDown)
                                           userInfo:nil
                                            repeats:YES];
    
    //let pebble know to show questions
    NSDictionary *update = @{ @(0):[NSNumber numberWithUint8:42]};
    
    [watch appMessagesPushUpdate:update onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
        if (!error) {
            //on the watch, 3 choices should be displayed.
            NSLog(@"Successfully sent message.");
        }
        else {
            NSLog(@"Error sending message: %@", error);
        }
    }];
    
    //update question label
    current++;
    NSLog(@"numQuestions: %d current: %d ", numQuestions, current);

    if(numQuestions == current){

    }
    else{
        NSLog(@"");
        _quesiton.text = questions[current];
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    NSLog(@"test");//grabs any touch event anywhere on the screen to do something with
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _pickerData = @[@"10", @"20", @"30", @"40", @"50"];
    
    //50 questions
    questions = @[@"How many years are there in a millennium?",
                  @"What is the fastest land animal",
                  @"The Beatles music band featured how many members",
                  @"Scarlet is a shade of which color?",
                  @"What galaxy is Earth located in?",
                  @"What is the first element on the periodic table of elements?",
                  @"Fauntleroy is the middle name of which cartoon character?",
                  @"How many sides does a hexagon have?",
                  @"What is Harry Potter's middle name?",
                  @"Bees create what sweet substance?",
                  @"A baby goat is called which of the following?",
                  @"How many colours are in a rainbow?",
                  @"What is the name of the bear in The Jungle Book?",
                  @"Which English king had six wives?",
                  @"What does Fred Flintstone wear around his neck?",
                  @"What is the name of Harry Potter's pet owl?",
                  @"Who is the patron saint of Ireland?",
                  @"What is the capital of England?",
                  @"Name Batman's crime fighting partner?",
                  @"What type of animal is Bullseye in the Toy Story films?",
                  @"What was the name of the monk in the Robin Hood legend?",
                  @"What is the name for the French dish of thin pancakes filled with sweet or spicy fillings?",
                  @"What season begins in the southern hemisphere around December 21?",
                  @"How many red stripes are on the U.S. flag?",
                  @"What is another word for 'deer meat'?",
                  @"Which country is the largest producer of bananas?",
                  @"What is another name for maize?",
                  @" What is the popular food used to carve jack-o-lanterns during Halloween?",
                  @"What is the name of the largest ocean on earth?",
                  @"What do you call molten rock before it has erupted?",
                  @"What do you call a person who studies rocks?",
                  @"What is the name of the highest mountain on earth?",
                  @"What is the second most common gas found in the air we breathe?",
                  @"What is the name of the phobia that involves an abnormal fear of spiders?",
                  @"What are female elephants called?",
                  @"A ‘doe’ is what kind of animal?",
                  @"How many legs does a spider have?",
                  @"The muscles found in the front of your thighs are known as what?",
                  @"What is the human body’s biggest organ?",
                  @"What are the bones that make up your spine are called?",
                  @"What is the shape of DNA?",
                  @"Nimbus, cumulus and stratus are types of what?",
                  @"Water freezes at what temperature?",
                  @"What is the longest river on Earth?",
                  @"The process of plants using energy from sunlight to turn carbon dioxide into food is known as what?",
                  @"How many horns did Triceratops have?",
                  @"What dinosaur themed book was turned into a blockbuster movie in 1993?",
                  @"The perimeter of a circle is also known as what?",
                  @"What country has the second largest population in the world?",
                  @"Tapas and paella are dishes that originated in what country?"];
    
    self.picker.dataSource = self;
    self.picker.delegate = self;
    
    watch = [[PBPebbleCentral defaultCentral] lastConnectedWatch];
    NSLog(@"Last connected watch: %@", watch);
    
    //launch the pebble app on the watch
    [watch appMessagesLaunch:^(PBWatch *watch, NSError *error) {
        if (!error) {
            NSLog(@"Successfully launched app.");
        }
        else {
            NSLog(@"Error launching app - Error: %@", error);
        }
    }
     ];
    
    //check that the watch supports 2 way messaging.
    [watch appMessagesGetIsSupported:^(PBWatch *watch, BOOL isAppMessagesSupported) {
        if (isAppMessagesSupported) {
            NSLog(@"This Pebble supports app message!");
        }
        else {
            NSLog(@":( - This Pebble does not support app message!");
        }
    }
    ];
    
    
    //Called when the watch sends us a message.
    [watch appMessagesAddReceiveUpdateHandler:^BOOL(PBWatch *watch, NSDictionary *update) {
        
        if([@"y" isEqualToString:[update objectForKey:@5]]){
            NSLog(@"yes");

            //points logic
            points = points + ((3 + remainingCounts) / 4) + 2;
            NSString *pointSrt = [NSString stringWithFormat:@"%d",points];
            _points.text = pointSrt;
            
            //show feedback view
            _feedbacklabel.text = @"Correct";
            _feedbackview.hidden = NO;
            
            //invalidate timer
            [timer invalidate];
        }
        else{
            NSLog(@"no");
            
            //do points logic
            NSString *pointSrt = [NSString stringWithFormat:@"%d",points];
            _points.text = pointSrt;

            //show feedback view
            _feedbacklabel.text = @"Incorrect";
            _feedbackview.hidden = NO;

            //invalidate timer
            [timer invalidate];
        }
        
        if(first){
            NSLog(@"numQuestions: %d, current: %d", numQuestions, current);
            if(numQuestions == current+1){
                NSLog(@"could have not shown the stupid continue thing");
                
                NSLog(@"should have just unhide view");
                _doneView.hidden = false;
                
                NSString *pointSrt = [NSString stringWithFormat:@"%d",points];
                _score.text = pointSrt;
                NSLog(@"points string that was just sent: %@", pointSrt);
                
                //we are done, invalidate timer
                [timer invalidate];
                
                //Send pebble points
                NSDictionary *update = @{ @(2):pointSrt};
                
                [watch appMessagesPushUpdate:update onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
                    if (!error) {
                        NSLog(@"Successfully sent message.");
                    }
                    else {
                        NSLog(@"Error sending message: %@", error);
                    }
                }];
                
            }
        }
        if(numQuestions == current+1){
            first = true;
        }
        return YES;
    }];
}

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}


// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    NSLog(@"row: %ld", (long)row);
    numQuestions = (row + 1) * 10;
    NSLog(@"numQuestions: %ld", (long)numQuestions);

}



- (IBAction)goHome:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    game = false;
    [timer invalidate];
}
- (IBAction)newGame:(id)sender {
    game = true;
    [self performSegueWithIdentifier: @"gameSegue" sender: self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
